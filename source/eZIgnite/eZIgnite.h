#pragma once

///
/// Function definitions.
///

void 
__cdecl 
DrawOverlayScene(
	_In_ void* UserData
);

void 
__cdecl 
DrawGameScene(
	_In_ void* UserData
);

bool 
__cdecl 
AwObjectLoop(
	_In_ void* Object, 
	_In_opt_ void* UserData
);