//
// This lets us use Windows types, structures, and defined API.
//
#include <Windows.h>

//
// To use mutable string objects.
//
#include <string>
#include <sstream>

//
// C++-specific containers.
//
#include <map>

//
// In order to use the Rift API, we include this.
//
#include "../sdkapi.h"

//
// Forward declaration of the necessary functions.
//
#include "eZIgnite.h"
#include <vector>

//
// This stores a pointer to all the SDK functions we can use. This is 
// intentionally a global so that other functions can use it.
//
PSDK_CONTEXT SDK_CONTEXT_GLOBAL;


//Enemy struct
struct sEnemy
{
	char* cName;
	bool bUseIgnite;

};

// Local Player
void* vLocalPlayer;

// Player TeamID
int pTeamID;

// Slot from Ignite
int IgniteSlot;

// Fix to load one time the Enemys
bool bEnemysLoaded = false;

//Enemys in list
std::vector<sEnemy> enemyList;

BOOL 
WINAPI 
DllMain(
	_In_ HINSTANCE hinstDLL, 
	_In_ DWORD fdwReason, 
	_In_ LPVOID lpvReserved
)
/*++

Routine Description:

	This function is called when the SDK loads the module. Consider
	this your module entry point.

Arguments:

	hinstDLL - The same value as lpvReserved when loaded by the SDK.

	fdwReason - Exclusively DLL_PROCESS_ATTACH (1) when loaded by the
		SDK.

	lpvReserved - The same value as hinstDLL when loaded by the SDK.
		Contains a pointer to the SDK_CONTEXT structure that can be
		retrieved via the SDK_EXTRACT_CONTEXT macro. This structure
		contains all the necessary function pointers required to
		interface with the Rift SDK.

Return Value:

	'TRUE' indicates module load success. 'FALSE' indicates module
	load failure.

--*/
{
	UNREFERENCED_PARAMETER(hinstDLL);

	//
	// We're only interested when the DLL attaches to the process.
	//
	if (fdwReason != DLL_PROCESS_ATTACH)
		return TRUE;

	//
	// This macro extracts the pointer to the SDK context from the lpvReserved
	// parameter.
	//
	SDK_EXTRACT_CONTEXT(lpvReserved);
	if (!SDK_CONTEXT_GLOBAL)
		return FALSE;

	//
	// Every module loaded by Rift should have a call to SdkNotifyLoadedModule
	// as soon as possible to ensure that other SDK API can be called.
	//
	if (!SDKSTATUS_SUCCESS(SdkNotifyLoadedModule("eZIgnite", SDK_VERSION)))
	{
		//
		// This routine will fail if a module of the same name is already
		// loaded or if there's a mismatch with the target SDK version.
		//
		return FALSE;
	}

	//
	// Retrieve the local player object.
	//
	SdkGetLocalPlayer(&vLocalPlayer);

	//
	// Retrieve the team for the local player.
	// 
	SdkGetObjectTeamID(vLocalPlayer, &pTeamID);
	

	// get the Ignite slot
	for (int i = 0; i < SPELL_SLOT_MAX; ++i)
	{
		SDK_SPELL Spell;
		SdkGetAISpell(vLocalPlayer, i, &Spell);
		if (strcmp(Spell.DisplayName, "game_spell_displayname_SummonerDot") == 0)
		{
			IgniteSlot = Spell.Slot;
			break;
		}
	}
	//
	// When the overlay is being drawn (e.g. hack menu is up), invoke
	// our function.
	//
	SdkRegisterOverlayScene(DrawOverlayScene, NULL);

	//
	// Each frame, the SDK will call into this function where we can 
	// add draw additional visuals before it's dispatched to the GPU
	// to render.
	//
	SdkRegisterGameScene(DrawGameScene, NULL);

	return TRUE;
}

void
__cdecl
DrawOverlayScene(
	_In_ void* UserData
)
/*++

Routine Description:

	This function is invoked every frame when the overlay is visible.

Arguments:

	UserData - A pointer to an arbitrary data structure, provided
		by the user, when the overlay scene was registered.

Return Value:

	None.

--*/ 
{
	UNREFERENCED_PARAMETER(UserData);
	
	//
	// Settings for enemys.
	//
	bool bEnemysExpanded = true;
	SdkUiBeginTree("Heroes", &bEnemysExpanded);
	if (bEnemysExpanded)
	{
		for (int i = 0;i < enemyList.size();i++)
		{
			// fu this string handle in c++ ;D
			std::string tmpText = "Use Ignite on ";
			tmpText += enemyList[i].cName;
			SdkUiCheckbox(tmpText.c_str(), &enemyList[i].bUseIgnite, NULL);
		}

		SdkUiEndTree();
	}
}

void
__cdecl
DrawGameScene(
	_In_ void* UserData
)
/*++

Routine Description:

	This function is invoked every frame.

Arguments:

	UserData - A pointer to an arbitrary data structure, provided
		by the user, when the overlay scene was registered.

Return Value:

	None.

--*/
{
	UNREFERENCED_PARAMETER(UserData);

	//
	// Enumerate game objects.
	//
	SdkEnumGameObjects(AwObjectLoop, NULL);
	if (!bEnemysLoaded)
		bEnemysLoaded = true;
}


// calc Ignite damage per level
int
GetIgniteDamag()
{
	int tmpPlayerLevel;
	SdkGetHeroExperience(vLocalPlayer, NULL, &tmpPlayerLevel);
	return 55 + tmpPlayerLevel * 25;
}

static
void 
ProcessHero(
	_In_ void* Object,
	_In_ SDKVECTOR& Position,
	_In_ int TeamID
)
/*++

Routine Description:

	This function processes a hero object.

Arguments:

	Object - The hero object.

	TeamID - The object's team.

Return Value:

	None.

--*/
{
	UNREFERENCED_PARAMETER(TeamID);

	//
	// Only Enemys
	//
	if (TeamID == pTeamID)
		return;

	if (!bEnemysLoaded)
	{
		sEnemy tmpEnemyStruct;
		tmpEnemyStruct.bUseIgnite = true;
		const char* tmpNameConst = NULL;
		if (SDKSTATUS_SUCCESS(SdkGetAIName(Object, &tmpNameConst)))
		{		
			char* tmpName = _strdup(tmpNameConst);
			tmpEnemyStruct.cName = tmpName;
			enemyList.push_back(tmpEnemyStruct);
		}
	}
	

	//
	// Is the target dead?
	//
	bool bDead;
	SdkIsObjectDead(Object, &bDead);
	if (bDead)
		return;

	//
	// Im dead?
	//
	SdkIsObjectDead(vLocalPlayer, &bDead);
	if (bDead)
		return;

	// get Ignite infos
	SDK_SPELL Spell;
	SdkGetAISpell(vLocalPlayer, IgniteSlot, &Spell);
	float Time;
	SdkGetGameTime(&Time);
	// return when Ignite is on CD
	if (Time < Spell.CooldownExpires)
		return;

	// Position and Distance handle
	SDKVECTOR tmpPlayerPosition;
	SdkGetObjectPosition(vLocalPlayer, &tmpPlayerPosition);

	struct Vector3
	{
		float x = 0;
		float y = 0;
		float z = 0;
	};

	Vector3 vector;
	vector.x = tmpPlayerPosition.x - Position.x;
	vector.y = tmpPlayerPosition.y - Position.y;
	vector.z = tmpPlayerPosition.z - Position.z;
	float fDistance = sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);
	//SdkUiConsoleWrite("distance: %f", fDistance);
	// stop when ur not in range
	if (fDistance > 600)
		return;
	int igniteDMG = GetIgniteDamag();
	SDK_HEALTH enemyHealth;
	SdkGetUnitHealth(Object, &enemyHealth);
	// Cast ignite when Ignite makes more dmg than target health
	if (enemyHealth.Current < igniteDMG)
	{
		bool canCast = false;
		SdkCanAICast(Object, &canCast);
		if (canCast && enemyHealth.Current <= igniteDMG)
		{
			const char* tmpNameConst = NULL;
			if (SDKSTATUS_SUCCESS(SdkGetAIName(Object, &tmpNameConst)))
			{
				char* tmpName = _strdup(tmpNameConst);
				for (int i = 0;i < enemyList.size();i++)
				{
					if (strcmp(tmpName, enemyList[i].cName) == 0)
					{
						if (enemyList[i].bUseIgnite)
							SdkCastSpellLocalPlayer(Object, NULL, (unsigned char)IgniteSlot, SPELL_CAST_START);
					}
				}
			}
		}
	}
}

bool 
__cdecl 
AwObjectLoop(
	_In_ void* Object, 
	_In_opt_ void* UserData
)
/*++

Routine Description:

	This function is invoked for each game object in the frame.

Arguments:
	
	Object - The game object.

	UserData - A pointer to an arbitrary data structure, provided
		by the user, when the overlay scene was registered.

Return Value:

	To stop iterating game objects, return false. To continue iterating
	over game objects, return true.

--*/
{
	UNREFERENCED_PARAMETER(UserData);

	//
	// We're not interested in non-attackable units.
	//
	if (!SDKSTATUS_SUCCESS(SdkIsObjectUnit(Object)))
		return true;

	//
	// Get the team for the object.
	//
	int TeamID;
	SdkGetObjectTeamID(Object, &TeamID);

	//
	// Get the position for the object.
	//
	SDKVECTOR Position;
	SdkGetObjectPosition(Object, &Position);


	//
	// Process all champions.
	//
	if (SDKSTATUS_SUCCESS(SdkIsObjectHero(Object)))
		ProcessHero(Object, Position, TeamID);

	return true;
}

